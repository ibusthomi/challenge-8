import React from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import LoginPage from "./pages/login";
import RegisterPage from "./pages/register";
import AllRoom from "./pages/allRoom";
import ErrorPage from "./pages/404";
import Navbar from "./pages/navbar";
import Test from "./pages/testing/test";
import CreateRoom from "./pages/createRoom/createRoom";
import VsCom from "./pages/vsCom/vsCom";
import VsPlayer from "./pages/pvp/pvp";
import GameHistory from "./pages/gameHistory";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function App() {
  const ProtectedRoute = ({ children }) => {
    const accessToken = localStorage.getItem("accessToken");
    const notify = () =>
      toast.warn("Please login First🙏🏻🙏🏻", {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "dark",
      });
    if (!accessToken) {
      notify();
      return <Navigate to="/login" />;
    } else {
      return children;
    }
  };

  return (
    <>
      <Routes>
        <Route path="/" element={<Navbar />}>
          <Route
            path=""
            element={
              <ProtectedRoute>
                <AllRoom />
              </ProtectedRoute>
            }
          />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
          <Route
            path="/all-rooms"
            element={
              <ProtectedRoute>
                <AllRoom />
              </ProtectedRoute>
            }
          />
          <Route path="/create-room" element={<CreateRoom />} />
          <Route path="/vs-com" element={<VsCom />} />
          <Route path="/join-room" element={<VsPlayer />} />
          <Route path="/history" element={<GameHistory />} />
          <Route path="*" element={<ErrorPage />} />
        </Route>
        <Route path="/test" element={<Test />} />
      </Routes>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="dark"
      />
    </>
  );
}

import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
// import { createBrowserRouter, RouterProvider } from "react-router-dom";

// import LoginPage from "./pages/login";
// import RegisterPage from "./pages/register";
// import AllRoom from "./pages/allRoom";
// import ErrorPage from "./pages/404";
// import Test from "./pages/test";

// const router = createBrowserRouter([
//   {
//     path: "/",
//     element: (
//       <div className="flex justify-center min-h-screen font-bold text-2xl items-center">
//         Ini halaman Root
//       </div>
//     ),
//     errorElement: <ErrorPage />,
//   },
// ]);
//   {
//     path: "/login",
//     element: <LoginPage />,
//   },
//   {
//     path: "/register",
//     element: <RegisterPage />,
//   },
//   {
//     path: "/allRoom",
//     element: <AllRoom />,
//   },
//   {
//     path: "/test",
//     element: <Test />,
//   },
// ]);

// const root = ReactDOM.createRoot(document.getElementById("root"));
// root.render(
//   <React.StrictMode>
//     <RouterProvider router={router} />
//   </React.StrictMode>
// );

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
);

import HistoryBox from "../Components/Fragments/HistoryBox";

const GameHistory = () => {
  return (
    <div className="flex w-full h-screen bg-slate-700">
      <div className="flex flex-col justify-center w-1/4 pb-1 px-10 mt-10 pt-8">
        <HistoryBox>VS COM</HistoryBox>
        <HistoryBox>VS COM</HistoryBox>
        <HistoryBox>ROOM SATU</HistoryBox>
        <HistoryBox>ROOM DUA</HistoryBox>
        <HistoryBox>ROOM TIGA</HistoryBox>
      </div>

      <div className="flex w-2/4">
        <div className="flex w-full mt-10 justify-center">
          <div className="flex flex-col w-full text-center px-10">
            <h1 className="text-5xl font-semibold text-white mb-20">
              GAME HISTORY
            </h1>
            <HistoryBox className="mt-1">7 JULI 2023 - 21.01</HistoryBox>
            <HistoryBox>7 JULI 2023 - 21.01</HistoryBox>
            <HistoryBox>7 JULI 2023 - 22.05</HistoryBox>
            <HistoryBox>7 JULI 2023 - 22.15</HistoryBox>
            <HistoryBox>7 JULI 2023 - 23.00</HistoryBox>
          </div>
        </div>
      </div>

      <div className="flex w-1/4">
        <div className="flex flex-col justify-center w-full pb-1 px-10 mt-10 pt-8">
          <HistoryBox>WIN</HistoryBox>
          <HistoryBox>DRAW</HistoryBox>
          <HistoryBox>DRAW</HistoryBox>
          <HistoryBox>LOSE</HistoryBox>
          <HistoryBox>WIN</HistoryBox>
        </div>
      </div>
    </div>
  );
};

export default GameHistory;

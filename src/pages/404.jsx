const ErrorPage = () => {
  return (
    <div className="flex flex-col justify-center h-screen text-2xl items-center bg-slate-700">
      <h1 className="text-3xl font-bold text-white">Sorry!</h1>
      <p className="my-5 text-white">An unexpected error has occurred</p>
      <p className="text-white">404 Page Not Found</p>
    </div>
  );
};

export default ErrorPage;

import "./pvp.css";
import RpsImage from "../../Components/Fragments/RpsImage/RpsImage";
import { useState } from "react";

const VsPlayer = () => {
  const [userChoice, setUserChoice] = useState("");
  return (
    <div className="flex w-full h-screen bg-slate-700">
      <div className="flex w-2/5 h-full flex-row justify-center">
        <div className="playerChoice">
          <h1 className="text-4xl font-semibold text-white ">PLAYER 1</h1>
          <RpsImage src="./assets/images/batu.png" id="batu" />
          <RpsImage src="./assets/images/gunting.png" id="gunting" />
          <RpsImage src="./assets/images/kertas.png" id="kertas" />
        </div>
      </div>

      <div className="flex w-1/5 h-full justify-center">
        <div>
          <h1 className="roomName text-center text-2xl font-semibold text-white">
            ROOM NAME
          </h1>
          <div>
            <h1 className="judgement text-7xl font-bold text-white">VS</h1>
            <RpsImage
              src="./assets/images/refresh.png"
              id="kertas"
              classname="refresh hover:border-white hover:bg-white"
              onClick={() => {
                if (userChoice !== "") {
                  setUserChoice("");
                }
              }}
            />
          </div>
        </div>
      </div>

      <div className="flex w-2/5 h-full justify-center">
        <div className="playerChoice">
          <h1 className="text-lg font-semibold text-white ">
            Make Your Decision
          </h1>
          {/* <RpsImage src="./assets/images/batu.png" id="batu" />
          <RpsImage src="./assets/images/gunting.png" id="gunting" />
          <RpsImage src="./assets/images/kertas.png" id="kertas" /> */}
          <RpsImage
            src="./assets/images/batu.png"
            id="batu"
            classname={userChoice === "batu" ? "bg-white" : "bg-slate-700"}
            onClick={() => {
              if (userChoice === "") {
                setUserChoice("batu");
              }
            }}
          />
          <RpsImage
            src="./assets/images/gunting.png"
            id="gunting"
            classname={userChoice === "gunting" ? "bg-white" : "bg-slate-700"}
            onClick={() => {
              if (userChoice === "") {
                setUserChoice("gunting");
              }
            }}
          />
          <RpsImage
            src="./assets/images/kertas.png"
            id="kertas"
            classname={userChoice === "kertas" ? "bg-white" : "bg-slate-700"}
            onClick={() => {
              if (userChoice === "") {
                setUserChoice("kertas");
              }
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default VsPlayer;

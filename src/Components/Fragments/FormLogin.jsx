import InputForm from "../Elements/Input/index";
import Button from "../Elements/Button/Button";
import { useNavigate } from "react-router-dom";
import { useState } from "react";

const FormLogin = () => {
  const [username, setUsername] = useState(null || "");
  const [password, setPassword] = useState(null || "");
  const navigate = useNavigate();

  const handleSubmit = () => {
    const accessToken = (username, password);
    localStorage.setItem("accessToken", accessToken);
    navigate("/all-rooms");
  };

  return (
    <form action="">
      <InputForm
        label="Username"
        name="username"
        type="username"
        placeholder="insert username"
        onChange={(e) => {
          setUsername(e.target.value);
        }}
      />
      <InputForm
        label="Password"
        name="password"
        type="password"
        placeholder="********"
        onChange={(e) => {
          setPassword(e.target.value);
        }}
      />
      <Button
        variant="bg-blue-600 w-full text-slate-600 mt-3"
        onClick={handleSubmit}
      >
        Login
      </Button>
    </form>
  );
};
export default FormLogin;

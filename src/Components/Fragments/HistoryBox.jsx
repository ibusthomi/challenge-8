const HistoryBox = (props) => {
  const { className, children } = props;
  return (
    <h1
      className={`mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded bg-black ${className}`}
    >
      {children}
    </h1>
  );
};

export default HistoryBox;

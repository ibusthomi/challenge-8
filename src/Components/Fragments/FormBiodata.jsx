import InputForm from "../Elements/Input/index";

const FormBiodata = () => {
  return (
    <form action="">
      <InputForm
        label="Fullname"
        name="fullname"
        type="text"
        placeholder="insert your name here"
      />
      <InputForm
        label="Address"
        name="address"
        type="text"
        placeholder="insert address here"
      />
      <InputForm
        label="Age"
        name="age"
        type="number"
        placeholder="insert your age here"
      />
    </form>
  );
};
export default FormBiodata;

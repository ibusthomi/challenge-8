import InputForm from "../Elements/Input/index";
import Button from "../Elements/Button/Button";

const FormRegister = () => {
  return (
    <form action="">
      <InputForm
        label="Username"
        name="username"
        type="username"
        placeholder="insert username"
      />
      <InputForm
        label="Email"
        name="email"
        type="email"
        placeholder="example@mail.com"
      />
      <InputForm
        label="Password"
        name="password"
        type="password"
        placeholder="********"
      />
      <InputForm
        label="Confirm Password"
        name="confirmPassword"
        type="password"
        placeholder="********"
      />
      <Button variant="bg-blue-600 w-full text-slate-600 mt-3">Register</Button>
    </form>
  );
};
export default FormRegister;
